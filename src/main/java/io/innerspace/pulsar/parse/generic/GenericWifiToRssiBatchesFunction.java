package io.innerspace.pulsar.parse.generic;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.innerspace.common.location.model.GenericWifiPacket;
import io.innerspace.common.location.model.GenericWifiPackets;
import io.innerspace.common.location.model.MacAddress;
import io.innerspace.common.location.model.RssiBatch;
import io.innerspace.common.location.model.RssiBatches;
import io.innerspace.common.location.model.RssiData;
import io.innerspace.common.location.pulsar.helpers.SensorDataParser;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.Duration;
import java.time.Instant;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.LongSummaryStatistics;
import java.util.stream.Stream;
import java.util.zip.GZIPInputStream;
import org.apache.pulsar.functions.api.Record;
import org.apache.pulsar.functions.api.WindowContext;
import org.apache.pulsar.functions.api.WindowFunction;
import org.slf4j.Logger;

public class GenericWifiToRssiBatchesFunction implements WindowFunction<byte[], RssiBatches> {
  private static final ObjectMapper objectMapper = new ObjectMapper();

  static {
    objectMapper.registerModule(new JavaTimeModule());
    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
  }

  private String _outputTopicPrefix;

  @Override
  public RssiBatches process(Collection<Record<byte[]>> collection, WindowContext windowContext) {
    long startTime = System.currentTimeMillis();
    windowContext.recordMetric("count_calls", 1);

    Logger logger = windowContext.getLogger();
    Object paramMap = windowContext.getUserConfigMap().get("genericParseParams");
    GenericParseParams parameters = GenericParseParams.buildParam(objectMapper, paramMap, logger);
    _outputTopicPrefix = parameters.getOutputTopicPrefix();
    Duration bucketInterval = parameters.getMinimumRssiInterval();
    Duration headOffsetInterval = parameters.getHeadOffsetInterval();
    Duration tailOffsetInterval = parameters.getTailOffsetInterval();
    HashSet<String> receiverIds = parameters.getReceiverIds();
    if (receiverIds == null || receiverIds.isEmpty()) {
      logger.warn("Mac Addresses were not found in the configuration.");
    }

    SensorDataParser<GenericWifiPacket> parser =
        new SensorDataParser<>(
            bucketInterval,
            GenericWifiPacket::getTimestamp,
            GenericWifiPacket::getReceiverAddress,
            this::convertToRssiData,
            logger);
    // Take the collection of GenericWifiPackets of different sensors and flatten them
    // into a List<GenericWifiPacket>.
    List<GenericWifiPacket> gwps =
        parser.parse(collection, record -> convertToGenericWifiPackets(record, logger));
    LongSummaryStatistics stats = parser.getTimestampStats(gwps);

    long rangeExpected =
        bucketInterval.plus(headOffsetInterval).plus(tailOffsetInterval).toMillis();
    long rangeActual = (stats.getMax() + tailOffsetInterval.toMillis()) - stats.getMin();
    if (rangeActual < rangeExpected) {
      logger.warn(
          "Not enough GenericWifiPackets to create valid RssiBatches | "
              + "RangeExpected : {} | RangeActual : {} | Count : {} |  From : {} | To : {}",
          Duration.ofMillis(rangeExpected),
          Duration.ofMillis(rangeActual),
          stats.getCount(),
          Instant.ofEpochMilli(stats.getMin()),
          Instant.ofEpochMilli(stats.getMax()));
      return null;
    }

    logger.info(
        "======================================================================================================");
    logger.info(
        "Data Range = Expected: {} | Actual: {} | From: {} | To: {}",
        rangeExpected,
        rangeActual,
        Instant.ofEpochMilli(stats.getMin()),
        Instant.ofEpochMilli(stats.getMax()).plus(bucketInterval));
    logger.info(
        "------------------------------------------------------------------------------------------------------");

    Instant validStart = Instant.ofEpochMilli(stats.getMin()).plus(headOffsetInterval);
    Instant validEnd = validStart.plus(bucketInterval);
    RssiBatches rssiBatches = parser.getRssiBatches(gwps, validStart, validEnd, receiverIds);

    long elapsedTime = System.currentTimeMillis() - startTime;
    windowContext.recordMetric("time_ms", elapsedTime);
    return rssiBatches;
  }

  private RssiBatch createRssiBatch(
      Instant fromTime,
      Duration bucketInterval,
      MacAddress rx,
      List<RssiData> rssiData,
      Logger logger) {
    Instant toTime = fromTime.plus(bucketInterval);
    RssiBatch rssiBatch = new RssiBatch(fromTime, toTime, rx.toString(), rssiData);
    logger.info(
        "Published RssiBatch({}). From: {}. To: {}. Count: {}",
        rssiBatch.getSensorId(),
        fromTime,
        toTime,
        rssiData.size());
    return rssiBatch;
  }

  private Stream<GenericWifiPacket> convertToGenericWifiPackets(
      Record<byte[]> record, Logger logger) {
    try (GZIPInputStream gis = new GZIPInputStream(new ByteArrayInputStream(record.getValue()));
        BufferedReader bf = new BufferedReader(new InputStreamReader(gis, "UTF-8"))) {
      StringBuilder sb = new StringBuilder();
      String line;
      while ((line = bf.readLine()) != null) {
        sb.append(line);
      }
      GenericWifiPackets packets = objectMapper.readValue(sb.toString(), GenericWifiPackets.class);
      return packets.getWifiPackets().stream();
    } catch (IOException e) {
      logger.error("Failed to deserialize byte[] into GenericWifiPackets", e);
    }
    return Stream.empty();
  }

  private RssiData convertToRssiData(GenericWifiPacket genericWifiPacket) {
    return new RssiData(
        genericWifiPacket.getTransmitterAddress(),
        genericWifiPacket.getTimestamp(),
        genericWifiPacket.getRssi(),
        genericWifiPacket.getChannel(),
        genericWifiPacket.getTypeSubtype());
  }

  public String getOutputTopicPrefix() {
    return _outputTopicPrefix;
  }
}
