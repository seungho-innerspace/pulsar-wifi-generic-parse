package io.innerspace.pulsar.parse.generic;

import io.innerspace.common.location.model.RssiBatch;
import io.innerspace.common.location.model.RssiBatches;
import io.innerspace.common.location.pulsar.serde.RssiBatchSerDe;
import java.util.Collection;
import java.util.concurrent.CompletableFuture;
import org.apache.pulsar.functions.api.Record;
import org.apache.pulsar.functions.api.WindowContext;
import org.apache.pulsar.functions.api.WindowFunction;

public class GenericWifiToRssiBatchFunction implements WindowFunction<byte[], Void> {
  private final GenericWifiToRssiBatchesFunction batchConverter;

  private GenericWifiToRssiBatchFunction() {
    batchConverter = new GenericWifiToRssiBatchesFunction();
  }

  @Override
  public Void process(Collection<Record<byte[]>> collection, WindowContext windowContext)
      throws Exception {
    long startTime = System.currentTimeMillis();

    RssiBatches rssiBatches = batchConverter.process(collection, windowContext);
    if (rssiBatches == null) return null;
    String outputTopicPrefix = batchConverter.getOutputTopicPrefix();
    // Dispatch each of the RssiBatches to their corresponding
    // output topic which gets built dynamically based on the receiver's mac address.
    CompletableFuture[] futures =
        rssiBatches
            .getRssiBatches()
            .parallelStream()
            .map(rssiBatch -> dispatch(windowContext, outputTopicPrefix, rssiBatch))
            .toArray(CompletableFuture[]::new);
    // Wait for all the messages to be dispatched successfully
    CompletableFuture.allOf(futures).join();

    long elapsedTime = System.currentTimeMillis() - startTime;
    windowContext.recordMetric("time_ms", elapsedTime);
    return null;
  }

  private CompletableFuture<Void> dispatch(
      WindowContext windowContext, String outputTopicPrefix, RssiBatch rssiBatch) {
    String outputTopic = outputTopicPrefix + rssiBatch.getSensorId();
    return windowContext.publish(outputTopic, rssiBatch, RssiBatchSerDe.class.getName());
  }
}
