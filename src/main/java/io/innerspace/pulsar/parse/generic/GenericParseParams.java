package io.innerspace.pulsar.parse.generic;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.innerspace.common.location.pulsar.params.WifiParseParams;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import org.slf4j.Logger;

public class GenericParseParams extends WifiParseParams {
  private static final String ERROR_MSG =
      "Something is wrong with the GenericParseParams parameters!";

  public static GenericParseParams buildParam(
      ObjectMapper objectMapper, Object param, Logger logger) {
    if (param instanceof String) {
      try {
        return objectMapper.readValue((String) param, GenericParseParams.class);
      } catch (JsonProcessingException e) {
        logger.error(ERROR_MSG);
        throw new IllegalStateException(ERROR_MSG, e);
      }
    }

    if (!(param instanceof Map)) {
      logger.error(ERROR_MSG);
      throw new IllegalStateException(ERROR_MSG);
    }
    Map<String, Object> gpMap = (Map<String, Object>) param;

    GenericParseParams gp = new GenericParseParams();
    gp.setMinimumRssiInterval(Duration.parse((String) gpMap.get("minimumRssiInterval")));
    gp.setOutputTopicPrefix((String) gpMap.get("outputTopicPrefix"));
    gp.setHeadOffsetInterval(Duration.parse((String) gpMap.get("headOffsetInterval")));
    gp.setTailOffsetInterval(Duration.parse((String) gpMap.get("tailOffsetInterval")));
    gp.setReceiverIds(new HashSet<>((ArrayList<String>) gpMap.get("receiverIds")));
    return gp;
  }

  public GenericParseParams() {
  }

}
