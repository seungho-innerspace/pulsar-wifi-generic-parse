package io.innerspace.pulsar.parse.generic;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import io.innerspace.common.location.pulsar.serde.RssiBatchesSerDe;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;

import org.apache.logging.log4j.util.Strings;
import org.apache.pulsar.common.functions.FunctionConfig;
import org.apache.pulsar.common.functions.FunctionConfig.ProcessingGuarantees;
import org.apache.pulsar.common.functions.WindowConfig;
import org.apache.pulsar.common.util.ObjectMapperFactory;
import org.apache.pulsar.functions.LocalRunner;

public class DebugTester {

  public static void main(String[] args) throws Exception {
    String brokerServiceUrl = "pulsar://localhost:6650";
    FunctionConfig functionConfig = null;

    if (args != null && args.length > 0) {
      Path path = Paths.get(args[0]);
      File yamlFile = path.toFile();
      if (yamlFile.exists()) {
        ObjectMapper yaml = ObjectMapperFactory.createYaml();
        functionConfig = yaml.readValue(yamlFile, FunctionConfig.class);
      }
      if (args.length > 1 && Strings.isNotEmpty(args[1])) {
        brokerServiceUrl = args[1];
      }
    }

    if (functionConfig == null) {
      String functionName = "pulsar-wifi-generic-parse";
      String deploymentUuid = "6dc8ad7f-aa7f-4699-9c74-98f615b7bf42";
      String topicInput = "persistent://innerspace/ingest-wifi-generic-v1/" + deploymentUuid;
      String topicOutput = "persistent://innerspace/wifi-rssi-v1/" + deploymentUuid;
      String topicOutputPrefix = topicOutput + "-";
      String topicLog = "persistent://innerspace/function-logs/" + functionName;

      GenericParseParams gp = new GenericParseParams();
      gp.setMinimumRssiInterval(Duration.ofSeconds(11));
      gp.setOutputTopicPrefix(topicOutputPrefix);

      ObjectMapper yaml = ObjectMapperFactory.createYaml();

      functionConfig = new FunctionConfig();
      functionConfig.setTenant("innerspace");
      functionConfig.setNamespace(deploymentUuid);
      functionConfig.setName(functionName);
      functionConfig.setInputs(ImmutableList.of(topicInput));
      functionConfig.setOutput(topicOutput);
      functionConfig.setOutputSerdeClassName(RssiBatchesSerDe.class.getName());
      functionConfig.setLogTopic(topicLog);
      functionConfig.setClassName(GenericWifiToRssiBatchFunction.class.getName());
      functionConfig.setRuntime(FunctionConfig.Runtime.JAVA);
      functionConfig.setProcessingGuarantees(ProcessingGuarantees.EFFECTIVELY_ONCE);
      functionConfig.setUserConfig(ImmutableMap.of("genericParseParams",
          yaml.readValue(yaml.writeValueAsString(gp), Object.class)));
      functionConfig.setWindowConfig(new WindowConfig().setWindowLengthDurationMs(22000L));
    }

    LocalRunner localRunner = LocalRunner.builder()
        .functionConfig(functionConfig)
        .brokerServiceUrl(brokerServiceUrl)
        .build();
    localRunner.start(true);
  }

}
