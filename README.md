# Pulsar Function for converting GenericWifiPacket to RssiBatch #

This pulsar function is responsible for parsing GenericWifiPackets and creating RssiBatch(es) 
with rssi data grouped into time-buckets specified in the parameters.

### Testing ###

By running the function in [localrun mode](http://pulsar.apache.org/docs/en/functions-deploy/#local-run-mode) a pulsar function can be run on a pulsar intance on your local machine while subscribing and publishing data to a remote pulsar broker. 


    bin/pulsar-admin functions localrun
        --jar /pulsar-wifi-generic-parse/target/pulsar-wifi-generic-parse-1.0.0.jar
        --classname io.innerspace.pulsar.parse.generic.GenericWifiToRssiBatchFunction
        --tenant innerspace
        --namespace 6dc8ad7f-aa7f-4699-9c74-98f615b7bf42
        --name pulsar-wifi-generic-parse
        --log-topic persistent://innerspace/function-logs/pulsar-wifi-generic-parse
        --broker-service-url pulsar://dev.rogsw4rpb5jutkxnc3rffq4dyg.bx.internal.cloudapp.net:6650
        --inputs persistent://innerspace/ingest-wifi-generic-v1/6dc8ad7f-aa7f-4699-9c74-98f615b7bf42
        --user-config {"genericParseParams":{ "minimumRssiInterval":"PT11S", "outputTopicPrefix": "persistent://innerspace/wifi-rssi-v1/6dc8ad7f-aa7f-4699-9c74-98f615b7bf42-"}}


### Deploying ###

To deploy the function to Pulsar a "fat" jar needs to be packaged that contains the function class, its serialization/deserialization ([SerDe](https://pulsar.apache.org/docs/en/functions-develop/#serde)) classes, and dependencies.

 [Deploy Pulsar Functions](http://pulsar.apache.org/docs/en/functions-deploy/)

  

    Need to register schema to publish AVRO class. for dev, set enable autoupdate schema
    https://pulsar.apache.org/docs/en/schema-manage/#schema-autoupdate
    bin/pulsar-admin namespaces set-is-allow-auto-update-schema --enable tenant/namespace
